package uz.pdp.bookreciewsmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bookreciewsmicroservice.model.BookReview;

import java.util.Optional;
import java.util.UUID;

public interface BookReviewRepository extends JpaRepository<BookReview, UUID> {

    @Query(nativeQuery = true,
    value = "select cast(user_id as varchar)\n" +
            "from book_review\n" +
            "where book_id = :id\n" +
            "limit 1")
    UUID findBookReviewByBookId(UUID id);
}
