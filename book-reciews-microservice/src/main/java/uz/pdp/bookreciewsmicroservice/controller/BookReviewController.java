package uz.pdp.bookreciewsmicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bookreciewsmicroservice.service.BookReviewsService;
import dto.MessageDto;

@RestController
@RequestMapping("/api/book-reviews")
public class BookReviewController {
    @Autowired
    BookReviewsService bookReviewsService;

    @PostMapping
    HttpEntity<?> bookAddReviews(@RequestBody MessageDto messageDto) {
        return bookReviewsService.sendEmail(messageDto);
    }

//    @GetMapping("/{bookId}")
//    HttpEntity<?> getAuthorIdByBookId(@PathVariable UUID bookId) {
//        return bookReviewsService.getAuthorId(bookId);
//    }
}
