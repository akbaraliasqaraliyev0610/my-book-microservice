package uz.pdp.bookreciewsmicroservice.service;

import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bookreciewsmicroservice.model.BookReview;
import uz.pdp.bookreciewsmicroservice.repository.BookReviewRepository;
import dto.MessageDto;
import uz.pdp.clients.user.UserClient;
import uz.pdp.clients.user.dto.UserDto;

import java.util.UUID;

@Service
public class BookReviewsService {
    @Value("${spring.rabbitmq.exchange}")
    String exchange;

    @Value("${spring.rabbitmq.routing-key}")
    String routingKey;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    BookReviewRepository bookReviewRepository;

    @Autowired
    UserClient userClient;

    @SneakyThrows
    public HttpEntity<?>sendEmail(MessageDto messageDto){
        UserDto userByEmail = userClient.getUserByEmail("akbaraliiasqaraliyev@gmail.com");
        BookReview bookReview = new BookReview(userByEmail.getId(), messageDto.getAuthorId(), messageDto.getMessage());
        UUID id = bookReviewRepository.findBookReviewByBookId(messageDto.getBookId());
        if (id==null) {
            throw new NullPointerException();
        }
        System.out.println(id);
        messageDto.setAuthorId(id);
        bookReviewRepository.save(bookReview);
        rabbitTemplate.convertAndSend(exchange, routingKey, messageDto);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }


}
