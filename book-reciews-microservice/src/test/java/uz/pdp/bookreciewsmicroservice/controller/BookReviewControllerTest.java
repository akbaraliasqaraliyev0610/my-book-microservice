package uz.pdp.bookreciewsmicroservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.MessageDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import uz.pdp.bookreciewsmicroservice.repository.BookReviewRepository;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-it.properties")
@AutoConfigureMockMvc
class BookReviewControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    BookReviewRepository bookReviewRepository;

    UUID bookId = UUID.fromString("58be0dee-a554-418d-a50d-1345e130729a");
    UUID UserIdId = UUID.fromString("58be0dee-a554-418d-a50d-1345e130729a");
    @Test
    void bookAddReviews() throws Exception{
        MessageDto messageDto = new MessageDto(bookId, null, "test");

         ResultActions perform = mockMvc.perform(
                post("/api/book-reviews")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(messageDto))
        );

         perform.andExpect(status().isCreated());
    }
}