package com.example.usercollectionmicroservice.repository;

import com.example.usercollectionmicroservice.model.UserBookCollection;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserCollectionRepository extends JpaRepository<UserBookCollection, UUID> {

    @Query(nativeQuery = true,
    value = "select book_id\n" +
            "from user_book_collection\n" +
            "where user_id = :id")
    List<UUID> getByUserId(UUID id);
}
