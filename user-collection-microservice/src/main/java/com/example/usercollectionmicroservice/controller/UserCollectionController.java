package com.example.usercollectionmicroservice.controller;

import com.example.usercollectionmicroservice.services.UserCollectionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/user-collection")
public class UserCollectionController {
    @Autowired
    UserCollectionServices userCollectionServices;
    @PostMapping("/{userId}/{bookId}")
    HttpEntity<?> userAddBookCollection(@PathVariable UUID userId, @PathVariable UUID bookId) {
        return userCollectionServices.userAddBookCollection(userId, bookId);
    }

    @GetMapping
    HttpEntity<?>getAllCollection(@RequestParam(name = "page", defaultValue = "1") int page,
                                  @RequestParam(name = "size", defaultValue = "2") int size,
                                  @RequestParam(name = "search", defaultValue = "") String search,
                                  @RequestParam(name = "sort", defaultValue = "title") String sort,
                                  @RequestParam(name = "direction", defaultValue = "true") boolean direction){
        return ResponseEntity.ok(userCollectionServices.getAllCollection(page, size, search, sort, direction));
    }
}
