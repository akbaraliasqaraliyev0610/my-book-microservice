package com.example.usercollectionmicroservice.services;

import com.example.usercollectionmicroservice.model.UserBookCollection;
import com.example.usercollectionmicroservice.repository.UserCollectionRepository;
import dto.BookGetDto;
import dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.clients.book.BookClient;
import uz.pdp.clients.payload.ApiResponse;
import uz.pdp.clients.user.UserClient;

import java.util.List;
import java.util.UUID;

@Service
public class UserCollectionServices {
    @Autowired
    UserCollectionRepository userCollectionRepository;

    @Autowired
    UserClient userClient;

    @Autowired
    BookClient bookClient;

    public HttpEntity<?> userAddBookCollection(UUID userId, UUID bookId) {
        userCollectionRepository.save(new UserBookCollection(userId, bookId));
        return ResponseEntity.ok(new ApiResponse("success", true, "OK"));
    }

    public HttpEntity<?> getAllCollection(int page, int size, String search, String sort, boolean direction) {
        Pageable paging = PageRequest.of(page - 1, size, direction ? Sort.Direction.ASC : Sort.Direction.DESC, sort);
        UserDto userByEmail = userClient.getUserByEmail("akbaraliiasqaraliyev@gmail.com");
        List<UUID> byUserId = userCollectionRepository.getByUserId(userByEmail.getId());
        BookGetDto allById = bookClient.getAllById(byUserId);
        return ResponseEntity.ok(allById);
    }

}
