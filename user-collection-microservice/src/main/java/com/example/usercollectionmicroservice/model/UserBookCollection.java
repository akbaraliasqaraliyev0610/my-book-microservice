package com.example.usercollectionmicroservice.model;

import com.example.usercollectionmicroservice.model.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "user_book_collection")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserBookCollection extends AbsEntity {
    private UUID userId;
    private UUID bookId;
}
