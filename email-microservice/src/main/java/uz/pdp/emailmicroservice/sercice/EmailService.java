package uz.pdp.emailmicroservice.sercice;

import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;


import uz.pdp.clients.user.UserClient;
import dto.UserDto;
import dto.MessageDto;

@Service
public class EmailService {

    @Value("${my.mail}")
    String mail;

    @Autowired
    JavaMailSender javaMailSender;



    @Autowired
    UserClient userClient;

    @SneakyThrows
    public void sendMail(MessageDto messageDto) {
         UserDto userById = userClient.getUserById(messageDto.getAuthorId());
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mail);
        message.setTo(userById.getEmail());
        message.setSubject("REVIEWS");
        String messageText = messageDto.getMessage();
        message.setText(messageText);
        javaMailSender.send(message);
        System.out.println("send email");
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public void receiveMessage(MessageDto message){
        if (message.getAuthorId() == null) {
            System.out.println(message);
            return;
        }
        sendMail(message);
    }
}
