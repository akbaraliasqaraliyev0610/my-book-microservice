package uz.pdp.bookmicroservice.projection;

import java.util.UUID;

public interface BookProjection {
    UUID getId();

    String getDescription();

    String getTitle();
}
