package uz.pdp.bookmicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bookmicroservice.model.Author;
import uz.pdp.bookmicroservice.services.AuthorServices;
import uz.pdp.clients.userCollection.UserCollectionClient;

@RestController
@RequestMapping("/api/book/author")
public class AuthorController {

    @Autowired
    UserCollectionClient userCollectionClient;

    @Autowired
    AuthorServices authorServices;

    @PostMapping
    HttpEntity<?> saveNewBook(@RequestBody Author author) {
        return authorServices.saveAuthor(author);
    }

    @GetMapping
    HttpEntity<?> getAllAuthor(){
//        userCollectionClient.userAddBookCollection(null, null);
        return authorServices.getAllAuthor();
    }
}
