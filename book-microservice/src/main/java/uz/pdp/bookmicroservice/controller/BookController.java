package uz.pdp.bookmicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bookmicroservice.services.BookServices;
import uz.pdp.clients.book.BookDto;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/book")
public class BookController {

    @Autowired
    BookServices bookServices;

    @PostMapping
    HttpEntity<?> saveNewBook(@RequestBody BookDto bookDto) {
        return bookServices.saveNewBook(bookDto);
    }

    @GetMapping
    HttpEntity<?> getAllBook(){
        return bookServices.getAllBook();
    }

    @GetMapping("/{id}")
    HttpEntity<?> getById(@PathVariable UUID id){
        return bookServices.getById(id);
    }

    @GetMapping("/all")
    HttpEntity<?>getAllById(@RequestBody List<UUID> list){
        return bookServices.getAllBookById(list);
    }


}
