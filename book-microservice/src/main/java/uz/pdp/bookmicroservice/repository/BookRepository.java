package uz.pdp.bookmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.bookmicroservice.model.Book;

import java.util.UUID;

public interface BookRepository extends JpaRepository<Book, UUID> {
}
