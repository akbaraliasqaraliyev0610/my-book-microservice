package uz.pdp.bookmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.bookmicroservice.model.Author;

import java.util.UUID;

public interface AuthorRepository extends JpaRepository<Author, UUID> {
}
