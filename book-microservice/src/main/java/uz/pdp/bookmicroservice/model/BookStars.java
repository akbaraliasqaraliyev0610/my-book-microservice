package uz.pdp.bookmicroservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.bookmicroservice.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BookStars extends AbsEntity {

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    private UUID userId;

    private int stars;
}
