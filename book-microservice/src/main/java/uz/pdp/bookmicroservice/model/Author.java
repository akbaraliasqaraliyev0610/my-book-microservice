package uz.pdp.bookmicroservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.bookmicroservice.model.template.AbsEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "authors")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Author extends AbsEntity {
    private String fullName;
}
