package uz.pdp.bookmicroservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.bookmicroservice.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "books")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Book extends AbsEntity {
    private String title;
    private String description;
    @ManyToMany
    private List<Author> authors;
    private UUID userId;
}
