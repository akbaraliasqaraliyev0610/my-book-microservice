package uz.pdp.bookmicroservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bookmicroservice.model.Author;
import uz.pdp.bookmicroservice.model.Book;
import uz.pdp.bookmicroservice.payload.ApiResponse;
import uz.pdp.bookmicroservice.repository.AuthorRepository;
import uz.pdp.bookmicroservice.repository.BookRepository;
import uz.pdp.clients.book.BookDto;
import uz.pdp.clients.book.BookGetDto;
import uz.pdp.clients.user.UserClient;
import uz.pdp.clients.user.dto.UserDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BookServices {
    @Autowired
    BookRepository bookRepository;

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    UserClient userClient;

   public HttpEntity<?> getAllBook() {
       UserDto userByEmail = userClient.getUserByEmail("akbaraliiasqaraliyev@gmail.com");
       System.out.println("result: "+userByEmail);
       List<Book> books = bookRepository.findAll();
        ApiResponse apiResponse = new ApiResponse("success", true, books);

        return ResponseEntity.ok(apiResponse);
    }

   public HttpEntity<?> saveNewBook(BookDto bookDto) {
       UserDto userByEmail = userClient.getUserByEmail("akbaraliiasqaraliyev@gmail.com");
       Book book = new Book();
        if(bookDto.getId()!=null)
            book.setId(bookDto.getId());
        book.setDescription(bookDto.getDescription());
        book.setTitle(bookDto.getTitle());
        book.setUserId(userByEmail.getId());
        List<Author> authors = authorRepository.findAllById(bookDto.getAuthors());
        book.setAuthors(authors);
        book.setUserId(bookDto.getUserId());
         bookRepository.save(book);
        return ResponseEntity.ok(new ApiResponse("success", true, "OK"));
    }

    public HttpEntity<?> getById(UUID id) {
        Optional<Book> byId = bookRepository.findById(id);
        ApiResponse apiResponse;
        if (byId.isPresent()) {
            Book book = byId.get();
            apiResponse = new ApiResponse("success",true, book);
        }else
            apiResponse = new ApiResponse("error", false, null);

        return ResponseEntity.ok(apiResponse);
    }

    public HttpEntity<?> getAllBookById(List<UUID> list) {
        List<Book> allById = bookRepository.findAllById(list);
        List<BookGetDto> bookGetDtos = new ArrayList<>();
        for (Book book : allById) {
            BookGetDto bookGetDto = new BookGetDto(book.getId(),
                    book.getDescription(),
                    book.getTitle());
            bookGetDtos.add(bookGetDto);
        }
        return ResponseEntity.ok(bookGetDtos);
    }
}
