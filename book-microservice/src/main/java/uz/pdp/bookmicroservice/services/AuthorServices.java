package uz.pdp.bookmicroservice.services;

import com.ctc.wstx.shaded.msv_core.util.LightStack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bookmicroservice.model.Author;
import uz.pdp.bookmicroservice.payload.ApiResponse;
import uz.pdp.bookmicroservice.repository.AuthorRepository;

import java.util.List;

@Service
public class AuthorServices {


    @Autowired
    AuthorRepository authorRepository;

   public HttpEntity<?> getAllAuthor(){
       List<Author> authors = authorRepository.findAll();
       ApiResponse response = new ApiResponse("success", true, authors);
        return ResponseEntity.ok(response);
    }

   public HttpEntity<?> saveAuthor(Author author) {
        authorRepository.save(author);
        return ResponseEntity.ok(new ApiResponse("success", true, "OK"));
    }
}
