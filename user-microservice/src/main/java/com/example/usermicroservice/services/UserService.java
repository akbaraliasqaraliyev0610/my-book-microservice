package com.example.usermicroservice.services;

import com.example.usermicroservice.model.User;
import com.example.usermicroservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.clients.payload.ApiResponse;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

   public HttpEntity<?> findByEmail(String email) {
        User byEmail = userRepository.findByEmail(email);
        ApiResponse response = new ApiResponse();
       User user;
       //        response = new ApiResponse("success", true, user);
       //        response = new ApiResponse("success", true, null);
        return ResponseEntity.ok(byEmail);

    }

    public HttpEntity<?> findById(UUID id){
        Optional<User> byId = userRepository.findById(id);
        if (byId.isPresent()) {
            return ResponseEntity.ok(byId.get());
        }else
            return ResponseEntity.ok("fail");
    }
}
