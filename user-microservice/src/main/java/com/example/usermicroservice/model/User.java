package com.example.usermicroservice.model;

import com.example.usermicroservice.model.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class User extends AbsEntity {
    private String fullName;
    private String email;
    private String password;

}
