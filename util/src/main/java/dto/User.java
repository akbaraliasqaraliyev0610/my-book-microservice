package dto;

import dto.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User extends AbsEntity {
    private String fullName;
    private String email;
    private String password;
}
