package dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.clients.template.AbsEntity;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto extends AbsEntity {
    private String fullName;
    private String email;
    private String password;
}
