package projection;

import java.util.UUID;

public interface ReviewProjection {
    UUID getUserId();
    UUID getReviewId();
    String getReview();
    String getUserName();
}
