package projection;

import java.util.UUID;

public interface CollectionProjection {
    UUID getBookId();
    String getTitle();
}
