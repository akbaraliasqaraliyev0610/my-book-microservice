package uz.pdp.clients.gateway;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import uz.pdp.clients.payload.ApiResponse;

import java.util.UUID;

@FeignClient("gateway")
public interface GatewayService {
    @PostMapping("/api/gateway/{bookId}")
    ApiResponse collectionAddNewBook(@PathVariable UUID bookId);
}
