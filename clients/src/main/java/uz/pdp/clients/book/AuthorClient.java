package uz.pdp.clients.book;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import uz.pdp.clients.payload.ApiResponse;

@FeignClient("book")
public interface AuthorClient {
    @PostMapping(path = "/api/book/author")
        ApiResponse saveAuthor(@RequestBody AuthorDto authorDto);

    @GetMapping
    ApiResponse getAllAuthor();
}
