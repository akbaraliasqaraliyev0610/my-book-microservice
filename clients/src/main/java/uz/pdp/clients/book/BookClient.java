package uz.pdp.clients.book;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import uz.pdp.clients.payload.ApiResponse;

import java.util.List;
import java.util.UUID;

@FeignClient("book")
public interface BookClient {

    @PostMapping(path = "/api/book")
    ApiResponse saveBook(@RequestBody BookDto bookDto);

    @GetMapping(path = "/api/book")
    ApiResponse getAllBook();

    @GetMapping(path = "/api/book/{id}")
    ApiResponse getById(@PathVariable UUID id);

    @GetMapping(path = "/api/book/all")
    BookGetDto getAllById(@RequestBody List<UUID> list);
}
