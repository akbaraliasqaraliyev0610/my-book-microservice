package uz.pdp.clients.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookGetDto {
    private UUID       id;

    private String     description;

    private String     title;

}
