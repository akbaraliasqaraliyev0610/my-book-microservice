package uz.pdp.clients.book_reviews;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@FeignClient("book-reviews")
public interface BookReviewsClient {
    @PostMapping("/api/book-reviews")
    String sendEmail(@RequestBody MessageDto messageDto);

    @GetMapping("/api/book-reviews/{bookId}")
    UUID getAuthorIdByBookId(@PathVariable("bookId") UUID bookId);
}
