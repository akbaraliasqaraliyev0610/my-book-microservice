package uz.pdp.clients.template;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AbsEntity {

    private UUID id;

    private Timestamp createdAt;

    private Timestamp updatedAt;
}
