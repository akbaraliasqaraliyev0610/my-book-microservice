package uz.pdp.clients.userCollection;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import uz.pdp.clients.book.BookGetDto;
import uz.pdp.clients.payload.ApiResponse;

import java.util.UUID;


@FeignClient("user-collection")
public interface UserCollectionClient {
    @PostMapping(path = "/api/user-collection/{userId}/{bookId}")
    ApiResponse userAddBookCollection(@PathVariable UUID userId, @PathVariable UUID bookId);

    @GetMapping(path = "/api/user-collection")
    BookGetDto getAllCollection();

}
