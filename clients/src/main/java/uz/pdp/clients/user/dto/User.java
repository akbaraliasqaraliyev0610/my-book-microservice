package uz.pdp.clients.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.clients.template.AbsEntity;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User extends AbsEntity {
    private String fullName;
    private String email;
    private String password;
}
