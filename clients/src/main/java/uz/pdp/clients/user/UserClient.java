package uz.pdp.clients.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import uz.pdp.clients.user.dto.UserDto;

import java.util.UUID;

@FeignClient("user")
public interface UserClient {
    @GetMapping("/api/user/email/{email}")
    UserDto getUserByEmail(@PathVariable("email") String email);

    @GetMapping("/api/user/{id}")
    UserDto getUserById(@PathVariable("id") UUID id);
}
