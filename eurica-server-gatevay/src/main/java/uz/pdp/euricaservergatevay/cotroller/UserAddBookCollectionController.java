package uz.pdp.euricaservergatevay.cotroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.clients.book.BookDto;
import uz.pdp.euricaservergatevay.services.UserAddBookCollectionServices;

import java.util.UUID;

@RestController
@RequestMapping("/api/gateway")
public class UserAddBookCollectionController {

    @Autowired
    UserAddBookCollectionServices userAddBookCollectionServices;

    @PostMapping("/{bookId}")
    HttpEntity<?> userAddCollectionBook(@PathVariable UUID bookId) {
        return userAddBookCollectionServices.userAddCollectionBook(bookId);
    }

    @PostMapping
    HttpEntity<?> saveNewBook(@RequestBody BookDto bookDto) {
        return userAddBookCollectionServices.userAddNewBook(bookDto);
    }
}
