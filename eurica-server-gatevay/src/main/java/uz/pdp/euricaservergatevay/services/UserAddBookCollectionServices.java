package uz.pdp.euricaservergatevay.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.clients.book.BookClient;
import uz.pdp.clients.book.BookDto;
import uz.pdp.clients.payload.ApiResponse;
import uz.pdp.clients.user.UserClient;
import uz.pdp.clients.user.dto.UserDto;
import uz.pdp.clients.userCollection.UserCollectionClient;

import java.util.UUID;

@Service
public class UserAddBookCollectionServices {
    @Autowired
    @Qualifier("uz.pdp.clients.user.UserClient")
    UserClient userClient;

    @Autowired
    @Qualifier("uz.pdp.clients.userCollection.UserCollectionClient")
    UserCollectionClient userCollectionClient;

    @Autowired
    @Qualifier("uz.pdp.clients.book.BookClient")
    BookClient bookClient;

    public HttpEntity<?> userAddCollectionBook(UUID bookId) {
        UserDto userByEmail = userClient.getUserByEmail("akbaraliiasqaraliyev@gmail.com");

        System.out.println(userByEmail);
        userCollectionClient.userAddBookCollection(userByEmail.getId(), bookId);
        ApiResponse apiResponse = new ApiResponse("success", true, "OK");
        return ResponseEntity.ok(apiResponse);
    }

    public HttpEntity<?> userAddNewBook(BookDto bookDto) {
        UserDto userByEmail = userClient.getUserByEmail("akbaraliiasqaraliyev@gmail.com");
        System.out.println(bookDto);
        bookDto.setUserId(userByEmail.getId());
        System.out.println(bookDto);

        bookClient.saveBook(bookDto);
        ApiResponse apiResponse = new ApiResponse("success", true, "OK");
        return ResponseEntity.ok(apiResponse);
    }
}
